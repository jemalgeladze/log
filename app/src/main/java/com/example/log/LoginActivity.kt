package com.example.log

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity() {

    private lateinit var editTextEmailAddress: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPasswordrepeat: EditText
    private lateinit var buttonregister: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        registerListener()
    }
    private fun init() {
        editTextEmailAddress = findViewById(R.id.editTextEmailAdress)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPasswordrepeat = findViewById(R.id.editTextPasswordRepeate)
        buttonregister = findViewById(R.id.buttonRegister)
    }
    private fun registerListener() {

        buttonregister.setOnClickListener {

            val email = editTextEmailAddress.text.toString()
            val password = editTextPassword.text.toString()
            val repeatpassword = editTextPasswordrepeat.text.toString()

            if (!(email.isEmpty()) && !(password.isEmpty()) && !(repeatpassword.isEmpty()) && password == repeatpassword && password.length >= 9 && email.contains('@')  ){

                Toast.makeText(this, "bravo!!!", Toast.LENGTH_SHORT).show()

            }




        }



    }
}