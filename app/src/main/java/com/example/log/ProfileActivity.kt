package com.example.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.log.LoginActivity
import com.example.log.R
import com.google.firebase.auth.FirebaseAuth


class ProfileActivity(val PasswordCangeActivity: Class<*>) : AppCompatActivity() {


    private lateinit var buttonLogout: Button
    private lateinit var buttonPasswordChange: Button
    private lateinit var TextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
        registerListeners()
        TextView.text = FirebaseAuth.getInstance().currentUser?.uid
    }

    private fun init() {
        buttonLogout = findViewById(R.id.ButtonLogOut)
        buttonPasswordChange = findViewById(R.id.buttonpasswordChange)
        TextView = findViewById(R.id.textView)
    }

    private fun registerListeners() {
        buttonLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        buttonPasswordChange.setOnClickListener {
            startActivity(Intent(this, PasswordCangeActivity))
        }
    }
}